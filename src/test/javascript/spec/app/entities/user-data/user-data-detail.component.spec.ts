import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { MyGuardianTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { UserDataDetailComponent } from '../../../../../../main/webapp/app/entities/user-data/user-data-detail.component';
import { UserDataService } from '../../../../../../main/webapp/app/entities/user-data/user-data.service';
import { UserData } from '../../../../../../main/webapp/app/entities/user-data/user-data.model';

describe('Component Tests', () => {

    describe('UserData Management Detail Component', () => {
        let comp: UserDataDetailComponent;
        let fixture: ComponentFixture<UserDataDetailComponent>;
        let service: UserDataService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MyGuardianTestModule],
                declarations: [UserDataDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    UserDataService,
                    EventManager
                ]
            }).overrideComponent(UserDataDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserDataDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserDataService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new UserData(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.userData).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
