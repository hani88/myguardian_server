import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { MyGuardianTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CareTypeDetailComponent } from '../../../../../../main/webapp/app/entities/care-type/care-type-detail.component';
import { CareTypeService } from '../../../../../../main/webapp/app/entities/care-type/care-type.service';
import { CareType } from '../../../../../../main/webapp/app/entities/care-type/care-type.model';

describe('Component Tests', () => {

    describe('CareType Management Detail Component', () => {
        let comp: CareTypeDetailComponent;
        let fixture: ComponentFixture<CareTypeDetailComponent>;
        let service: CareTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MyGuardianTestModule],
                declarations: [CareTypeDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CareTypeService,
                    EventManager
                ]
            }).overrideComponent(CareTypeDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CareTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CareTypeService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CareType(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.careType).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
