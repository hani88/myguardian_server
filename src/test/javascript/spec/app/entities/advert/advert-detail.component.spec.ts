import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils, EventManager } from 'ng-jhipster';
import { MyGuardianTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AdvertDetailComponent } from '../../../../../../main/webapp/app/entities/advert/advert-detail.component';
import { AdvertService } from '../../../../../../main/webapp/app/entities/advert/advert.service';
import { Advert } from '../../../../../../main/webapp/app/entities/advert/advert.model';

describe('Component Tests', () => {

    describe('Advert Management Detail Component', () => {
        let comp: AdvertDetailComponent;
        let fixture: ComponentFixture<AdvertDetailComponent>;
        let service: AdvertService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MyGuardianTestModule],
                declarations: [AdvertDetailComponent],
                providers: [
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AdvertService,
                    EventManager
                ]
            }).overrideComponent(AdvertDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AdvertDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AdvertService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Advert(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.advert).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
