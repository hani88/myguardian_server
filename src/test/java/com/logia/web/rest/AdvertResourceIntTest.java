package com.logia.web.rest;

import com.logia.MyGuardianApp;

import com.logia.domain.Advert;
import com.logia.repository.AdvertRepository;
import com.logia.service.AdvertService;
import com.logia.service.UserDataService;
import com.logia.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.logia.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AdvertResource REST controller.
 *
 * @see AdvertResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyGuardianApp.class)
public class AdvertResourceIntTest {

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_JOB_SCOPE = "AAAAAAAAAA";
    private static final String UPDATED_JOB_SCOPE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_JOB_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JOB_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_JOB_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JOB_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_EXPIRY = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_EXPIRY = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private AdvertRepository advertRepository;

    @Autowired
    private AdvertService advertService;

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAdvertMockMvc;

    private Advert advert;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AdvertResource advertResource = new AdvertResource(advertService, userDataService);
        this.restAdvertMockMvc = MockMvcBuilders.standaloneSetup(advertResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advert createEntity(EntityManager em) {
        Advert advert = new Advert()
            .status(DEFAULT_STATUS)
            .jobScope(DEFAULT_JOB_SCOPE)
            .jobStartDate(DEFAULT_JOB_START_DATE)
            .jobEndDate(DEFAULT_JOB_END_DATE)
            .expiry(DEFAULT_EXPIRY);
        return advert;
    }

    @Before
    public void initTest() {
        advert = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvert() throws Exception {
        int databaseSizeBeforeCreate = advertRepository.findAll().size();

        // Create the Advert
        restAdvertMockMvc.perform(post("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isCreated());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeCreate + 1);
        Advert testAdvert = advertList.get(advertList.size() - 1);
        assertThat(testAdvert.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testAdvert.getJobScope()).isEqualTo(DEFAULT_JOB_SCOPE);
        assertThat(testAdvert.getJobStartDate()).isEqualTo(DEFAULT_JOB_START_DATE);
        assertThat(testAdvert.getJobEndDate()).isEqualTo(DEFAULT_JOB_END_DATE);
        assertThat(testAdvert.getExpiry()).isEqualTo(DEFAULT_EXPIRY);
    }

    @Test
    @Transactional
    public void createAdvertWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advertRepository.findAll().size();

        // Create the Advert with an existing ID
        advert.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvertMockMvc.perform(post("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAdverts() throws Exception {
        // Initialize the database
        advertRepository.saveAndFlush(advert);

        // Get all the advertList
        restAdvertMockMvc.perform(get("/api/adverts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advert.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].jobScope").value(hasItem(DEFAULT_JOB_SCOPE.toString())))
            .andExpect(jsonPath("$.[*].jobStartDate").value(hasItem(sameInstant(DEFAULT_JOB_START_DATE))))
            .andExpect(jsonPath("$.[*].jobEndDate").value(hasItem(sameInstant(DEFAULT_JOB_END_DATE))))
            .andExpect(jsonPath("$.[*].expiry").value(hasItem(sameInstant(DEFAULT_EXPIRY))));
    }

    @Test
    @Transactional
    public void getAdvert() throws Exception {
        // Initialize the database
        advertRepository.saveAndFlush(advert);

        // Get the advert
        restAdvertMockMvc.perform(get("/api/adverts/{id}", advert.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(advert.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.jobScope").value(DEFAULT_JOB_SCOPE.toString()))
            .andExpect(jsonPath("$.jobStartDate").value(sameInstant(DEFAULT_JOB_START_DATE)))
            .andExpect(jsonPath("$.jobEndDate").value(sameInstant(DEFAULT_JOB_END_DATE)))
            .andExpect(jsonPath("$.expiry").value(sameInstant(DEFAULT_EXPIRY)));
    }

    @Test
    @Transactional
    public void getNonExistingAdvert() throws Exception {
        // Get the advert
        restAdvertMockMvc.perform(get("/api/adverts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvert() throws Exception {
        // Initialize the database
        advertService.save(advert);

        int databaseSizeBeforeUpdate = advertRepository.findAll().size();

        // Update the advert
        Advert updatedAdvert = advertRepository.findOne(advert.getId());
        updatedAdvert
            .status(UPDATED_STATUS)
            .jobScope(UPDATED_JOB_SCOPE)
            .jobStartDate(UPDATED_JOB_START_DATE)
            .jobEndDate(UPDATED_JOB_END_DATE)
            .expiry(UPDATED_EXPIRY);

        restAdvertMockMvc.perform(put("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAdvert)))
            .andExpect(status().isOk());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeUpdate);
        Advert testAdvert = advertList.get(advertList.size() - 1);
        assertThat(testAdvert.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testAdvert.getJobScope()).isEqualTo(UPDATED_JOB_SCOPE);
        assertThat(testAdvert.getJobStartDate()).isEqualTo(UPDATED_JOB_START_DATE);
        assertThat(testAdvert.getJobEndDate()).isEqualTo(UPDATED_JOB_END_DATE);
        assertThat(testAdvert.getExpiry()).isEqualTo(UPDATED_EXPIRY);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvert() throws Exception {
        int databaseSizeBeforeUpdate = advertRepository.findAll().size();

        // Create the Advert

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAdvertMockMvc.perform(put("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isCreated());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAdvert() throws Exception {
        // Initialize the database
        advertService.save(advert);

        int databaseSizeBeforeDelete = advertRepository.findAll().size();

        // Get the advert
        restAdvertMockMvc.perform(delete("/api/adverts/{id}", advert.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Advert.class);
    }
}
