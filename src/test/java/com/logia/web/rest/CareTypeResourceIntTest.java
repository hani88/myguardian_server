package com.logia.web.rest;

import com.logia.MyGuardianApp;

import com.logia.domain.CareType;
import com.logia.repository.CareTypeRepository;
import com.logia.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CareTypeResource REST controller.
 *
 * @see CareTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyGuardianApp.class)
public class CareTypeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CareTypeRepository careTypeRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCareTypeMockMvc;

    private CareType careType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CareTypeResource careTypeResource = new CareTypeResource(careTypeRepository);
        this.restCareTypeMockMvc = MockMvcBuilders.standaloneSetup(careTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CareType createEntity(EntityManager em) {
        CareType careType = new CareType()
            .name(DEFAULT_NAME);
        return careType;
    }

    @Before
    public void initTest() {
        careType = createEntity(em);
    }

    @Test
    @Transactional
    public void createCareType() throws Exception {
        int databaseSizeBeforeCreate = careTypeRepository.findAll().size();

        // Create the CareType
        restCareTypeMockMvc.perform(post("/api/care-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(careType)))
            .andExpect(status().isCreated());

        // Validate the CareType in the database
        List<CareType> careTypeList = careTypeRepository.findAll();
        assertThat(careTypeList).hasSize(databaseSizeBeforeCreate + 1);
        CareType testCareType = careTypeList.get(careTypeList.size() - 1);
        assertThat(testCareType.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createCareTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = careTypeRepository.findAll().size();

        // Create the CareType with an existing ID
        careType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCareTypeMockMvc.perform(post("/api/care-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(careType)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<CareType> careTypeList = careTypeRepository.findAll();
        assertThat(careTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCareTypes() throws Exception {
        // Initialize the database
        careTypeRepository.saveAndFlush(careType);

        // Get all the careTypeList
        restCareTypeMockMvc.perform(get("/api/care-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(careType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getCareType() throws Exception {
        // Initialize the database
        careTypeRepository.saveAndFlush(careType);

        // Get the careType
        restCareTypeMockMvc.perform(get("/api/care-types/{id}", careType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(careType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCareType() throws Exception {
        // Get the careType
        restCareTypeMockMvc.perform(get("/api/care-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCareType() throws Exception {
        // Initialize the database
        careTypeRepository.saveAndFlush(careType);
        int databaseSizeBeforeUpdate = careTypeRepository.findAll().size();

        // Update the careType
        CareType updatedCareType = careTypeRepository.findOne(careType.getId());
        updatedCareType
            .name(UPDATED_NAME);

        restCareTypeMockMvc.perform(put("/api/care-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCareType)))
            .andExpect(status().isOk());

        // Validate the CareType in the database
        List<CareType> careTypeList = careTypeRepository.findAll();
        assertThat(careTypeList).hasSize(databaseSizeBeforeUpdate);
        CareType testCareType = careTypeList.get(careTypeList.size() - 1);
        assertThat(testCareType.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingCareType() throws Exception {
        int databaseSizeBeforeUpdate = careTypeRepository.findAll().size();

        // Create the CareType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCareTypeMockMvc.perform(put("/api/care-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(careType)))
            .andExpect(status().isCreated());

        // Validate the CareType in the database
        List<CareType> careTypeList = careTypeRepository.findAll();
        assertThat(careTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCareType() throws Exception {
        // Initialize the database
        careTypeRepository.saveAndFlush(careType);
        int databaseSizeBeforeDelete = careTypeRepository.findAll().size();

        // Get the careType
        restCareTypeMockMvc.perform(delete("/api/care-types/{id}", careType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CareType> careTypeList = careTypeRepository.findAll();
        assertThat(careTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CareType.class);
    }
}
