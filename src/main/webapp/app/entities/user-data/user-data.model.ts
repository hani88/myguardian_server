
const enum Role {
    'PARENT',
    'CARER',
    'ADMIN',
    'GOD'

};
import { Location } from '../location';
export class UserData {
    constructor(
        public id?: number,
        public phone?: string,
        public religion?: string,
        public kids?: number,
        public occupation?: string,
        public bio?: string,
        public picture?: any,
        public role?: Role,
        public rating?: number,
        public location?: Location,
    ) {
    }
}
