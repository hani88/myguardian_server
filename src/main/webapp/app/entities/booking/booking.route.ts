import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { BookingComponent } from './booking.component';
import { BookingDetailComponent } from './booking-detail.component';
import { BookingPopupComponent } from './booking-dialog.component';
import { BookingDeletePopupComponent } from './booking-delete-dialog.component';

import { Principal } from '../../shared';

export const bookingRoute: Routes = [
  {
    path: 'booking',
    component: BookingComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Bookings'
    },
    canActivate: [UserRouteAccessService]
  }, {
    path: 'booking/:id',
    component: BookingDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Bookings'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bookingPopupRoute: Routes = [
  {
    path: 'booking-new',
    component: BookingPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Bookings'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'booking/:id/edit',
    component: BookingPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Bookings'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'booking/:id/delete',
    component: BookingDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Bookings'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
