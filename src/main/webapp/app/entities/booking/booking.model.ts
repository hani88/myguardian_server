import { Advert } from '../advert';
export class Booking {
    constructor(
        public id?: number,
        public status?: string,
        public rating?: number,
        public salary?: number,
        public advert?: Advert,
    ) {
    }
}
