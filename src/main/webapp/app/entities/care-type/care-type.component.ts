import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, AlertService } from 'ng-jhipster';

import { CareType } from './care-type.model';
import { CareTypeService } from './care-type.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-care-type',
    templateUrl: './care-type.component.html'
})
export class CareTypeComponent implements OnInit, OnDestroy {
careTypes: CareType[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private careTypeService: CareTypeService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.careTypeService.query().subscribe(
            (res: Response) => {
                this.careTypes = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCareTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CareType) {
        return item.id;
    }
    registerChangeInCareTypes() {
        this.eventSubscriber = this.eventManager.subscribe('careTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
