import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager   } from 'ng-jhipster';

import { CareType } from './care-type.model';
import { CareTypeService } from './care-type.service';

@Component({
    selector: 'jhi-care-type-detail',
    templateUrl: './care-type-detail.component.html'
})
export class CareTypeDetailComponent implements OnInit, OnDestroy {

    careType: CareType;
    private subscription: any;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private careTypeService: CareTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCareTypes();
    }

    load(id) {
        this.careTypeService.find(id).subscribe((careType) => {
            this.careType = careType;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCareTypes() {
        this.eventSubscriber = this.eventManager.subscribe('careTypeListModification', (response) => this.load(this.careType.id));
    }
}
