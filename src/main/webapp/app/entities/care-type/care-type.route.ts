import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { CareTypeComponent } from './care-type.component';
import { CareTypeDetailComponent } from './care-type-detail.component';
import { CareTypePopupComponent } from './care-type-dialog.component';
import { CareTypeDeletePopupComponent } from './care-type-delete-dialog.component';

import { Principal } from '../../shared';

export const careTypeRoute: Routes = [
  {
    path: 'care-type',
    component: CareTypeComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'CareTypes'
    },
    canActivate: [UserRouteAccessService]
  }, {
    path: 'care-type/:id',
    component: CareTypeDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'CareTypes'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const careTypePopupRoute: Routes = [
  {
    path: 'care-type-new',
    component: CareTypePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'CareTypes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'care-type/:id/edit',
    component: CareTypePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'CareTypes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'care-type/:id/delete',
    component: CareTypeDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'CareTypes'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
