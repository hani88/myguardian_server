import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager } from 'ng-jhipster';

import { CareType } from './care-type.model';
import { CareTypePopupService } from './care-type-popup.service';
import { CareTypeService } from './care-type.service';

@Component({
    selector: 'jhi-care-type-delete-dialog',
    templateUrl: './care-type-delete-dialog.component.html'
})
export class CareTypeDeleteDialogComponent {

    careType: CareType;

    constructor(
        private careTypeService: CareTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.careTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'careTypeListModification',
                content: 'Deleted an careType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-care-type-delete-popup',
    template: ''
})
export class CareTypeDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private careTypePopupService: CareTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.careTypePopupService
                .open(CareTypeDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
