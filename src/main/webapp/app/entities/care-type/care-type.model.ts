export class CareType {
    constructor(
        public id?: number,
        public name?: string,
    ) {
    }
}
