import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyGuardianSharedModule } from '../../shared';
import {
    CareTypeService,
    CareTypePopupService,
    CareTypeComponent,
    CareTypeDetailComponent,
    CareTypeDialogComponent,
    CareTypePopupComponent,
    CareTypeDeletePopupComponent,
    CareTypeDeleteDialogComponent,
    careTypeRoute,
    careTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...careTypeRoute,
    ...careTypePopupRoute,
];

@NgModule({
    imports: [
        MyGuardianSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CareTypeComponent,
        CareTypeDetailComponent,
        CareTypeDialogComponent,
        CareTypeDeleteDialogComponent,
        CareTypePopupComponent,
        CareTypeDeletePopupComponent,
    ],
    entryComponents: [
        CareTypeComponent,
        CareTypeDialogComponent,
        CareTypePopupComponent,
        CareTypeDeleteDialogComponent,
        CareTypeDeletePopupComponent,
    ],
    providers: [
        CareTypeService,
        CareTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyGuardianCareTypeModule {}
