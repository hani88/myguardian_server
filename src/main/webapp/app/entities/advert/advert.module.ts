import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyGuardianSharedModule } from '../../shared';
import {
    AdvertService,
    AdvertPopupService,
    AdvertComponent,
    AdvertDetailComponent,
    AdvertDialogComponent,
    AdvertPopupComponent,
    AdvertDeletePopupComponent,
    AdvertDeleteDialogComponent,
    advertRoute,
    advertPopupRoute,
} from './';

const ENTITY_STATES = [
    ...advertRoute,
    ...advertPopupRoute,
];

@NgModule({
    imports: [
        MyGuardianSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AdvertComponent,
        AdvertDetailComponent,
        AdvertDialogComponent,
        AdvertDeleteDialogComponent,
        AdvertPopupComponent,
        AdvertDeletePopupComponent,
    ],
    entryComponents: [
        AdvertComponent,
        AdvertDialogComponent,
        AdvertPopupComponent,
        AdvertDeleteDialogComponent,
        AdvertDeletePopupComponent,
    ],
    providers: [
        AdvertService,
        AdvertPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyGuardianAdvertModule {}
