import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager   } from 'ng-jhipster';

import { Advert } from './advert.model';
import { AdvertService } from './advert.service';

@Component({
    selector: 'jhi-advert-detail',
    templateUrl: './advert-detail.component.html'
})
export class AdvertDetailComponent implements OnInit, OnDestroy {

    advert: Advert;
    private subscription: any;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private advertService: AdvertService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAdverts();
    }

    load(id) {
        this.advertService.find(id).subscribe((advert) => {
            this.advert = advert;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAdverts() {
        this.eventSubscriber = this.eventManager.subscribe('advertListModification', (response) => this.load(this.advert.id));
    }
}
