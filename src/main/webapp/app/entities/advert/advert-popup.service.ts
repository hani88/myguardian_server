import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Advert } from './advert.model';
import { AdvertService } from './advert.service';
@Injectable()
export class AdvertPopupService {
    private isOpen = false;
    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private advertService: AdvertService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.advertService.find(id).subscribe((advert) => {
                advert.jobStartDate = this.datePipe
                    .transform(advert.jobStartDate, 'yyyy-MM-ddThh:mm');
                advert.jobEndDate = this.datePipe
                    .transform(advert.jobEndDate, 'yyyy-MM-ddThh:mm');
                advert.expiry = this.datePipe
                    .transform(advert.expiry, 'yyyy-MM-ddThh:mm');
                this.advertModalRef(component, advert);
            });
        } else {
            return this.advertModalRef(component, new Advert());
        }
    }

    advertModalRef(component: Component, advert: Advert): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.advert = advert;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
