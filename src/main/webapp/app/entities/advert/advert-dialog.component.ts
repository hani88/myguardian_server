import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { Advert } from './advert.model';
import { AdvertPopupService } from './advert-popup.service';
import { AdvertService } from './advert.service';
import { CareType, CareTypeService } from '../care-type';
import { UserData, UserDataService } from '../user-data';

@Component({
    selector: 'jhi-advert-dialog',
    templateUrl: './advert-dialog.component.html'
})
export class AdvertDialogComponent implements OnInit {

    advert: Advert;
    authorities: any[];
    isSaving: boolean;

    types: CareType[];

    userdata: UserData[];
    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private advertService: AdvertService,
        private careTypeService: CareTypeService,
        private userDataService: UserDataService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.careTypeService.query({filter: 'advert-is-null'}).subscribe((res: Response) => {
            if (!this.advert.type || !this.advert.type) {
                this.types = res.json();
            } else {
                this.careTypeService.find(this.advert.type).subscribe((subRes: CareType) => {
                    this.types = [subRes].concat(res.json());
                }, (subRes: Response) => this.onError(subRes.json()));
            }
        }, (res: Response) => this.onError(res.json()));
        this.userDataService.query().subscribe(
            (res: Response) => { this.userdata = res.json(); }, (res: Response) => this.onError(res.json()));
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.advert.id !== undefined) {
            this.advertService.update(this.advert)
                .subscribe((res: Advert) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
        } else {
            this.advertService.create(this.advert)
                .subscribe((res: Advert) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
        }
    }

    private onSaveSuccess(result: Advert) {
        this.eventManager.broadcast({ name: 'advertListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackCareTypeById(index: number, item: CareType) {
        return item.id;
    }

    trackUserDataById(index: number, item: UserData) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-advert-popup',
    template: ''
})
export class AdvertPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private advertPopupService: AdvertPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.advertPopupService
                    .open(AdvertDialogComponent, params['id']);
            } else {
                this.modalRef = this.advertPopupService
                    .open(AdvertDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
