import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager } from 'ng-jhipster';

import { Advert } from './advert.model';
import { AdvertPopupService } from './advert-popup.service';
import { AdvertService } from './advert.service';

@Component({
    selector: 'jhi-advert-delete-dialog',
    templateUrl: './advert-delete-dialog.component.html'
})
export class AdvertDeleteDialogComponent {

    advert: Advert;

    constructor(
        private advertService: AdvertService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.advertService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'advertListModification',
                content: 'Deleted an advert'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-advert-delete-popup',
    template: ''
})
export class AdvertDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private advertPopupService: AdvertPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.advertPopupService
                .open(AdvertDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
