import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { AdvertComponent } from './advert.component';
import { AdvertDetailComponent } from './advert-detail.component';
import { AdvertPopupComponent } from './advert-dialog.component';
import { AdvertDeletePopupComponent } from './advert-delete-dialog.component';

import { Principal } from '../../shared';

export const advertRoute: Routes = [
  {
    path: 'advert',
    component: AdvertComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Adverts'
    },
    canActivate: [UserRouteAccessService]
  }, {
    path: 'advert/:id',
    component: AdvertDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Adverts'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const advertPopupRoute: Routes = [
  {
    path: 'advert-new',
    component: AdvertPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Adverts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'advert/:id/edit',
    component: AdvertPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Adverts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'advert/:id/delete',
    component: AdvertDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'Adverts'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
