import { CareType } from '../care-type';
import { UserData } from '../user-data';
export class Advert {
    constructor(
        public id?: number,
        public status?: string,
        public jobScope?: string,
        public jobStartDate?: any,
        public jobEndDate?: any,
        public expiry?: any,
        public type?: number,
        public creator?: UserData,
    ) {
    }
}
