import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Advert } from './advert.model';
import { DateUtils } from 'ng-jhipster';
@Injectable()
export class AdvertService {

    private resourceUrl = 'api/adverts';

    constructor(private http: Http, private dateUtils: DateUtils) { }

    create(advert: Advert): Observable<Advert> {
        const copy: Advert = Object.assign({}, advert);
        copy.jobStartDate = this.dateUtils.toDate(advert.jobStartDate);
        copy.jobEndDate = this.dateUtils.toDate(advert.jobEndDate);
        copy.expiry = this.dateUtils.toDate(advert.expiry);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(advert: Advert): Observable<Advert> {
        const copy: Advert = Object.assign({}, advert);

        copy.jobStartDate = this.dateUtils.toDate(advert.jobStartDate);

        copy.jobEndDate = this.dateUtils.toDate(advert.jobEndDate);

        copy.expiry = this.dateUtils.toDate(advert.expiry);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Advert> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            jsonResponse.jobStartDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.jobStartDate);
            jsonResponse.jobEndDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.jobEndDate);
            jsonResponse.expiry = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.expiry);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<Response> {
        const options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertResponse(res))
        ;
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: any): any {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].jobStartDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].jobStartDate);
            jsonResponse[i].jobEndDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].jobEndDate);
            jsonResponse[i].expiry = this.dateUtils
                .convertDateTimeFromServer(jsonResponse[i].expiry);
        }
        res._body = jsonResponse;
        return res;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        const options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            const params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('query', req.query);

            options.search = params;
        }
        return options;
    }
}
