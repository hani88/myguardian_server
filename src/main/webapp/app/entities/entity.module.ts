import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MyGuardianUserDataModule } from './user-data/user-data.module';
import { MyGuardianCountryModule } from './country/country.module';
import { MyGuardianAdvertModule } from './advert/advert.module';
import { MyGuardianLocationModule } from './location/location.module';
import { MyGuardianCareTypeModule } from './care-type/care-type.module';
import { MyGuardianBookingModule } from './booking/booking.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MyGuardianUserDataModule,
        MyGuardianCountryModule,
        MyGuardianAdvertModule,
        MyGuardianLocationModule,
        MyGuardianCareTypeModule,
        MyGuardianBookingModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyGuardianEntityModule {}
