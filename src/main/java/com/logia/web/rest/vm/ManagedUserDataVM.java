package com.logia.web.rest.vm;

import com.logia.domain.Location;
import com.logia.domain.enumeration.Role;
import com.logia.service.dto.UserDataDTO;

/**
 * Created by TREXX on 01/07/2017.
 */
public class ManagedUserDataVM extends UserDataDTO{

    public UserDataDTO uddto;


    public ManagedUserDataVM(Long id, String phone, String religion, Integer kids, String occupation, String bio, byte[] picture, String pictureContentType, Role role, Long rating, Location location) {
        super(id, phone, religion, kids, occupation, bio, picture, pictureContentType, role, rating, location);
    }

    @Override
    public String toString() {
        return "ManagedUserDataVM{" +
            "}" + super.toString();
    }
}
