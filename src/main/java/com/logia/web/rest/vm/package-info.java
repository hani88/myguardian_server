/**
 * View Models used by Spring MVC REST controllers.
 */
package com.logia.web.rest.vm;
