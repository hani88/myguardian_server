package com.logia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.logia.domain.Advert;
import com.logia.domain.AdvertClone;
import com.logia.domain.UserData;
import com.logia.service.AdvertService;
import com.logia.service.UserDataService;
import com.logia.web.rest.util.HeaderUtil;
import com.logia.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Advert.
 */
@RestController
@RequestMapping("/api")
public class AdvertResource {

    private final Logger log = LoggerFactory.getLogger(AdvertResource.class);

    private static final String ENTITY_NAME = "advert";

    private final AdvertService advertService;
    private final UserDataService userDataService;

    public AdvertResource(AdvertService advertService, UserDataService userDataService) {
        this.advertService = advertService;
        this.userDataService = userDataService;
    }

    /**
     * POST  /adverts : Create a new advert.
     *
     * @param advert the advert to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advert, or with status 400 (Bad Request) if the advert has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adverts")
    @Timed
    public ResponseEntity<Advert> createAdvert(@RequestBody Advert advert) throws URISyntaxException {
        log.debug("REST request to save Advert : {}", advert);
        if (advert.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new advert cannot already have an ID")).body(null);
        }
        Advert result = advertService.save(advert);
        return ResponseEntity.created(new URI("/api/adverts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /adverts-new : Create a new advert.
     *
     * @param advert the advert to create
     * @return the ResponseEntity with status 201 (Created) and with body the new advert, or with status 400 (Bad Request) if the advert has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/adverts-new")
    @Timed
    public ResponseEntity<AdvertClone> createAdvert(@RequestBody AdvertClone advert) throws URISyntaxException {
        log.debug("REST request to save Advert : {}", advert);
        if (advert.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new advert cannot already have an ID")).body(null);
        }
        Advert result = advertService.save(advert.toAdvert());
        return ResponseEntity.created(new URI("/api/adverts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result.toClone());
    }

    /**
     * PUT  /adverts : Updates an existing advert.
     *
     * @param advert the advert to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advert,
     * or with status 400 (Bad Request) if the advert is not valid,
     * or with status 500 (Internal Server Error) if the advert couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adverts")
    @Timed
    public ResponseEntity<Advert> updateAdvert(@RequestBody Advert advert) throws URISyntaxException {
        log.debug("REST request to update Advert : {}", advert);
        if (advert.getId() == null) {
            return createAdvert(advert);
        }
        Advert result = advertService.save(advert);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advert.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /adverts : Updates an existing advert.
     *
     * @param advert the advert to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated advert,
     * or with status 400 (Bad Request) if the advert is not valid,
     * or with status 500 (Internal Server Error) if the advert couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/adverts-new")
    @Timed
    public ResponseEntity<AdvertClone> updateAdvert(@RequestBody AdvertClone advert) throws URISyntaxException {
        log.debug("REST request to update Advert : {}", advert);
        if (advert.getId() == null) {
            return createAdvert(advert);
        }
        Advert result = advertService.save(advert.toAdvert());
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, advert.getId().toString()))
            .body(result.toClone());
    }

    /**
     * GET  /adverts : get all the adverts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of adverts in body
     */
    @GetMapping("/adverts")
    @Timed
    public ResponseEntity<List<Advert>> getAllAdverts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Adverts");
        Page<Advert> page = advertService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/adverts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /adverts/:id : get the "id" advert.
     *
     * @param id the id of the advert to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the advert, or with status 404 (Not Found)
     */
    @GetMapping("/adverts/{id}")
    @Timed
    public ResponseEntity<Advert> getAdvert(@PathVariable Long id) {
        log.debug("REST request to get Advert : {}", id);
        Advert advert = advertService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(advert));
    }

    /**
     * DELETE  /adverts/:id : delete the "id" advert.
     *
     * @param id the id of the advert to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/adverts/{id}")
    @Timed
    public ResponseEntity<Void> deleteAdvert(@PathVariable Long id) {
        log.debug("REST request to delete Advert : {}", id);
        advertService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/adverts/findByReligion/{religion}")
    @Timed
    public List<Advert> FindGuardianAdvertByReligion(@PathVariable String religion) {
        log.debug("REST request to Guardian Advert based on Religion: {}", religion);
        List<Advert> advert = advertService.findByReligion(religion);
        return advert;
    }

    @GetMapping("/adverts/findActiveJobsForGuardian/{id}")
    @Timed
    public List<Advert> FindActivejobsForAGuardian(@PathVariable Long id) {
        log.debug("REST request to pull Guardian's ACTIVE Advert/job: {}", id);
        UserData user = userDataService.findOne(id);
        List<Advert> advert = advertService.getConfirmedJobsForGuardian(user);
        return advert;
    }


    @GetMapping("/adverts/findMyAds/{id}")
    @Timed
    public List<Advert> FindMyAds(@PathVariable Long id) {
        log.debug("REST request to Get users ads: {}");
        List<Advert> advert = advertService.findMyAds(id);
        return advert;
    }

    @GetMapping("/adverts/findAdsForGuardian/")
    @Timed
    public List<Advert> findAdsForGuardian() {
        log.debug("REST request to Get users ads: {}");
        List<Advert> advert = advertService.findAllAdsForGuardian();
        return advert;
    }

    @GetMapping("/adverts/findAdsForParent/")
    @Timed
    public List<Advert> findAdsForParent() {
        log.debug("REST request to Get parent ads: {}");
        List<Advert> advert = advertService.findAllAdsForParents();
        return advert;
    }
}
