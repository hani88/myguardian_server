package com.logia.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.logia.domain.CareType;

import com.logia.repository.CareTypeRepository;
import com.logia.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing CareType.
 */
@RestController
@RequestMapping("/api")
public class CareTypeResource {

    private final Logger log = LoggerFactory.getLogger(CareTypeResource.class);

    private static final String ENTITY_NAME = "careType";
        
    private final CareTypeRepository careTypeRepository;

    public CareTypeResource(CareTypeRepository careTypeRepository) {
        this.careTypeRepository = careTypeRepository;
    }

    /**
     * POST  /care-types : Create a new careType.
     *
     * @param careType the careType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new careType, or with status 400 (Bad Request) if the careType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/care-types")
    @Timed
    public ResponseEntity<CareType> createCareType(@RequestBody CareType careType) throws URISyntaxException {
        log.debug("REST request to save CareType : {}", careType);
        if (careType.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new careType cannot already have an ID")).body(null);
        }
        CareType result = careTypeRepository.save(careType);
        return ResponseEntity.created(new URI("/api/care-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /care-types : Updates an existing careType.
     *
     * @param careType the careType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated careType,
     * or with status 400 (Bad Request) if the careType is not valid,
     * or with status 500 (Internal Server Error) if the careType couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/care-types")
    @Timed
    public ResponseEntity<CareType> updateCareType(@RequestBody CareType careType) throws URISyntaxException {
        log.debug("REST request to update CareType : {}", careType);
        if (careType.getId() == null) {
            return createCareType(careType);
        }
        CareType result = careTypeRepository.save(careType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, careType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /care-types : get all the careTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of careTypes in body
     */
    @GetMapping("/care-types")
    @Timed
    public List<CareType> getAllCareTypes() {
        log.debug("REST request to get all CareTypes");
        List<CareType> careTypes = careTypeRepository.findAll();
        return careTypes;
    }

    /**
     * GET  /care-types/:id : get the "id" careType.
     *
     * @param id the id of the careType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the careType, or with status 404 (Not Found)
     */
    @GetMapping("/care-types/{id}")
    @Timed
    public ResponseEntity<CareType> getCareType(@PathVariable Long id) {
        log.debug("REST request to get CareType : {}", id);
        CareType careType = careTypeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(careType));
    }

    /**
     * DELETE  /care-types/:id : delete the "id" careType.
     *
     * @param id the id of the careType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/care-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCareType(@PathVariable Long id) {
        log.debug("REST request to delete CareType : {}", id);
        careTypeRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
