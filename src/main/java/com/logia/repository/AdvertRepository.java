package com.logia.repository;

import com.logia.domain.Advert;
import com.logia.domain.UserData;
import org.springframework.data.jpa.repository.*;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Advert entity.
 */
@SuppressWarnings("unused")
public interface AdvertRepository extends JpaRepository<Advert,Long> {

    @Query(value = "SELECT ADVERT.* FROM ADVERT LEFT JOIN USER_DATA ON ADVERT.CREATOR_ID=USER_DATA.ID WHERE USER_DATA.RELIGION=?1", nativeQuery = true)
    default List<Advert> findGuardianAdvertsByReligion(String religion) {
        return null;
    }

    @Query(value = "SELECT ADVERT.* FROM ADVERT WHERE STATUS='ACTIVE' AND CREATOR_ID=?1", nativeQuery = true)
    List<Advert> findGuardianConfirmedJob(Long userdataID);

    @Query(value = "SELECT advert.* FROM advert WHERE STATUS in ('NEW','ACTIVE','COMPLETED') AND CREATOR_ID= ?1", nativeQuery = true)
    List<Advert> findMyAds(Long id);

    @Query(value = "SELECT advert.* FROM advert LEFT JOIN  user_data ON advert.creator_id=user_data.id WHERE STATUS in ('NEW','ACTIVE','COMPLETED') AND user_data.jhi_role='PARENT'", nativeQuery = true)
    List<Advert> findAdsforGuardian();

    @Query(value = "SELECT advert.* FROM advert LEFT JOIN  user_data ON advert.creator_id=user_data.id WHERE STATUS in ('NEW','ACTIVE','COMPLETED') AND user_data.jhi_role='CARER'", nativeQuery = true)
    List<Advert> findAdsForParents();


}
