package com.logia.repository;

import com.logia.domain.UserData;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserData entity.
 */
@SuppressWarnings("unused")
public interface UserDataRepository extends JpaRepository<UserData,Long> {

}
