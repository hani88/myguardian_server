package com.logia.repository;

import com.logia.domain.CareType;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the CareType entity.
 */
@SuppressWarnings("unused")
public interface CareTypeRepository extends JpaRepository<CareType,Long> {

}
