package com.logia.repository;

import com.logia.domain.Booking;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Booking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BookingRepository extends JpaRepository<Booking,Long> {

    @Query(value = "SELECT BOOKING.* FROM BOOKING WHERE CREATOR_ID= ?1", nativeQuery = true)
    List<Booking> findMyBooking(Long id);

    @Query(value = "SELECT BOOKING.* FROM BOOKING WHERE ADVERT_ID= ?1", nativeQuery = true)
    List<Booking> findBookingPerAdd(Long id);

}
