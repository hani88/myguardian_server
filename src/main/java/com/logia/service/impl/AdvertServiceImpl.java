package com.logia.service.impl;

import com.logia.domain.UserData;
import com.logia.repository.UserDataRepository;
import com.logia.service.AdvertService;
import com.logia.domain.Advert;
import com.logia.repository.AdvertRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Advert.
 */
@Service
@Transactional
public class AdvertServiceImpl implements AdvertService{

    private final Logger log = LoggerFactory.getLogger(AdvertServiceImpl.class);

    private final AdvertRepository advertRepository;
    private final UserDataRepository userDataRepository;

    public AdvertServiceImpl(AdvertRepository advertRepository, UserDataRepository userDataRepository) {
        this.advertRepository = advertRepository;
        this.userDataRepository = userDataRepository;
    }

    /**
     * Save a advert.
     *
     * @param advert the entity to save
     * @return the persisted entity
     */
    @Override
    public Advert save(Advert advert) {
        log.debug("Request to save Advert : {}", advert);
        Advert result = advertRepository.save(advert);
        return result;
    }

    /**
     *  Get all the adverts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Advert> findAll(Pageable pageable) {
        log.debug("Request to get all Adverts");
        Page<Advert> result = advertRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one advert by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Advert findOne(Long id) {
        log.debug("Request to get Advert : {}", id);
        Advert advert = advertRepository.findOne(id);
        return advert;
    }

    /**
     *  Delete the  advert by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Advert : {}", id);
        advertRepository.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Advert> findByReligion(String religion) {
        log.debug("Request to get Advert : {}");
        List<Advert> advert = advertRepository.findGuardianAdvertsByReligion(religion);

        return advert;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Advert> getConfirmedJobsForGuardian(UserData user) {
        log.debug("Request to get jobs for user : {}", user.getId());
        List<Advert> advert = advertRepository.findGuardianConfirmedJob(user.getId());

        return advert;
    }

    @Override
    public List<Advert> findMyAds(Long id) {
        List<Advert> advert = advertRepository.findMyAds(id);
        return advert;
    }

    @Override
    public List<Advert> findAllAdsForGuardian() {
        List<Advert> advert = advertRepository.findAdsforGuardian();
        return advert;
    }

    @Override
    public List<Advert> findAllAdsForParents() {
        List<Advert> advert = advertRepository.findAdsForParents();
        return advert;
    }
}
