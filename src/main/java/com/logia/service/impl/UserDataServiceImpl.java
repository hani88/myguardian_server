package com.logia.service.impl;

import com.logia.service.UserDataService;
import com.logia.domain.UserData;
import com.logia.repository.UserDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing UserData.
 */
@Service
@Transactional
public class UserDataServiceImpl implements UserDataService{

    private final Logger log = LoggerFactory.getLogger(UserDataServiceImpl.class);

    private final UserDataRepository userDataRepository;

    public UserDataServiceImpl(UserDataRepository userDataRepository) {
        this.userDataRepository = userDataRepository;
    }

    /**
     * Save a userData.
     *
     * @param userData the entity to save
     * @return the persisted entity
     */
    @Override
    public UserData save(UserData userData) {
        log.debug("Request to save UserData : {}", userData);
        UserData result = userDataRepository.save(userData);
        return result;
    }

    /**
     *  Get all the userData.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<UserData> findAll() {
        log.debug("Request to get all UserData");
        List<UserData> result = userDataRepository.findAll();

        return result;
    }

    /**
     *  Get one userData by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UserData findOne(Long id) {
        log.debug("Request to get UserData : {}", id);
        UserData userData = userDataRepository.findOne(id);

        return userData;
    }

    /**
     *  Delete the  userData by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserData : {}", id);
        userDataRepository.delete(id);
    }
}
