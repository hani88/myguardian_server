package com.logia.service;

import com.logia.domain.Advert;
import com.logia.domain.UserData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Advert.
 */
public interface AdvertService {

    /**
     * Save a advert.
     *
     * @param advert the entity to save
     * @return the persisted entity
     */
    Advert save(Advert advert);

    /**
     *  Get all the adverts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Advert> findAll(Pageable pageable);

    /**
     *  Get the "id" advert.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Advert findOne(Long id);

    /**
     *  Delete the "id" advert.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     *  Find the advert based on religion.
     *
     *  @param religion of the userData
     */
    List<Advert> findByReligion(String religion);

    /**
        *  Find the confirmed jobs based on guardian.
        *
        *  @param user of the userData
     */
    List<Advert> getConfirmedJobsForGuardian(UserData user);


    List<Advert> findMyAds(Long id);


    List<Advert> findAllAdsForGuardian();

    List<Advert> findAllAdsForParents();
}
