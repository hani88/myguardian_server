package com.logia.service.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;


import java.io.IOException;
import java.text.SimpleDateFormat;

import java.util.TimeZone;

/**
 * Created by TREXX on 19/09/2017.
 */
public class CustomDateSerializer extends JsonDeserializer {

    final DateTimeFormatter dateFormat;
    TimeZone tz = TimeZone.getDefault();

    public CustomDateSerializer() {
        dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZ");
        dateFormat.withOffsetParsed();
    }

    @Override
    public DateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String date = jsonParser.getText();

            return dateFormat.parseDateTime(date);
    }

}
