package com.logia.service.dto;

import com.logia.domain.Location;
import com.logia.domain.enumeration.Role;

import java.util.Arrays;

/**
 * Created by TREXX on 01/07/2017.
 */
public class UserDataDTO {

    private Long id;

    private String phone;

    private String religion;

    private Integer kids;

    private String occupation;

    private String bio;

    private byte[] picture;

    private String pictureContentType;

    private Role role;

    private Long rating;

    private Location location;

    public UserDataDTO() {
    }

    public UserDataDTO(Long id, String phone, String religion, Integer kids, String occupation, String bio, byte[] picture, String pictureContentType, Role role, Long rating, Location location) {
        this.id = id;
        this.phone = phone;
        this.religion = religion;
        this.kids = kids;
        this.occupation = occupation;
        this.bio = bio;
        this.picture = picture;
        this.pictureContentType = pictureContentType;
        this.role = role;
        this.rating = rating;
        this.location = location;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public Integer getKids() {
        return kids;
    }

    public void setKids(Integer kids) {
        this.kids = kids;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getPictureContentType() {
        return pictureContentType;
    }

    public void setPictureContentType(String pictureContentType) {
        this.pictureContentType = pictureContentType;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "UserDataDTO{" +
            "id=" + id +
            ", phone='" + phone + '\'' +
            ", religion='" + religion + '\'' +
            ", kids=" + kids +
            ", occupation='" + occupation + '\'' +
            ", bio='" + bio + '\'' +
            ", role=" + role +
            ", rating=" + rating +
            ", location=" + location +
            '}';
    }


}
