package com.logia.service;

import com.logia.domain.UserData;
import java.util.List;

/**
 * Service Interface for managing UserData.
 */
public interface UserDataService {

    /**
     * Save a userData.
     *
     * @param userData the entity to save
     * @return the persisted entity
     */
    UserData save(UserData userData);

    /**
     *  Get all the userData.
     *  
     *  @return the list of entities
     */
    List<UserData> findAll();

    /**
     *  Get the "id" userData.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UserData findOne(Long id);

    /**
     *  Delete the "id" userData.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
