package com.logia.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Booking.
 */
@Entity
@Table(name = "booking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "rating")
    private Long rating;

    @Column(name = "salary")
    private Long salary;

    @OneToOne
    @JoinColumn(name = "advert_id")
    private Advert advert;

    @ManyToOne
    private UserData creator;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public Booking status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRating() {
        return rating;
    }

    public Booking rating(Long rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Long getSalary() {
        return salary;
    }

    public Booking salary(Long salary) {
        this.salary = salary;
        return this;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Advert getAdvert() {
        return advert;
    }

    public Booking advert(Advert advert) {
        this.advert = advert;
        return this;
    }

    public void setAdvert(Advert advert) {
        this.advert = advert;
    }

    public UserData getCreator() {
        return creator;
    }

    public Booking creator(UserData userData) {
        this.creator = userData;
        return this;
    }

    public void setCreator(UserData userData) {
        this.creator = userData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Booking booking = (Booking) o;
        if (booking.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), booking.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Booking{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", rating='" + getRating() + "'" +
            ", salary='" + getSalary() + "'" +
            "}";
    }
}
