package com.logia.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.TimeZone;

/**
 * A Advert.
 */
@Entity
@Table(name = "advert")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Advert implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "job_scope")
    private String jobScope;

    @Column(name = "job_start_date")
    private ZonedDateTime jobStartDate;

    @Column(name = "job_end_date")
    private ZonedDateTime jobEndDate;

    @Column(name = "expiry")
    private ZonedDateTime expiry;

    @Column(name = "type_id")
    private Long type;

    @ManyToOne
    private UserData creator;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public Advert status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJobScope() {
        return jobScope;
    }

    public Advert jobScope(String jobScope) {
        this.jobScope = jobScope;
        return this;
    }

    public void setJobScope(String jobScope) {
        this.jobScope = jobScope;
    }

    public ZonedDateTime getJobStartDate() {
        return jobStartDate;
    }

    public Advert jobStartDate(ZonedDateTime jobStartDate) {
        this.jobStartDate = jobStartDate;
        return this;
    }

    public void setJobStartDate(ZonedDateTime jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public ZonedDateTime getJobEndDate() {
        return jobEndDate;
    }

    public Advert jobEndDate(ZonedDateTime jobEndDate) {
        this.jobEndDate = jobEndDate;
        return this;
    }

    public void setJobEndDate(ZonedDateTime jobEndDate) {
        this.jobEndDate = jobEndDate;
    }

    public ZonedDateTime getExpiry() {
        return expiry;
    }

    public Advert expiry(ZonedDateTime expiry) {
        this.expiry = expiry;
        return this;
    }

    public void setExpiry(ZonedDateTime expiry) {
        this.expiry = expiry;
    }

    public Long getType() {
        return type;
    }

    public Advert type(Long careType) {
        this.type = careType;
        return this;
    }

    public void setType(Long careType) {
        this.type = careType;
    }

    public UserData getCreator() {
        return creator;
    }

    public Advert creator(UserData userData) {
        this.creator = userData;
        return this;
    }

    public void setCreator(UserData userData) {
        this.creator = userData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Advert advert = (Advert) o;
        if (advert.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, advert.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Advert{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", jobScope='" + jobScope + "'" +
            ", jobStartDate='" + jobStartDate + "'" +
            ", jobEndDate='" + jobEndDate + "'" +
            ", expiry='" + expiry + "'" +
            '}';
    }

    public AdvertClone toClone() {

        AdvertClone advertc = new AdvertClone();
        if (this.getId()!=null){
            advertc.setId(this.getId());
        }

        advertc.setCreator(this.getCreator());
        ZoneId zid = this.getExpiry().getZone();
        advertc.setExpiry(new DateTime(this.getExpiry().toInstant().toEpochMilli(), DateTimeZone.forTimeZone(TimeZone.getTimeZone(zid))));
        advertc.setJobEndDate(new DateTime(this.getJobEndDate().toInstant().toEpochMilli(), DateTimeZone.forTimeZone(TimeZone.getTimeZone(zid))));
        advertc.setJobStartDate(new DateTime(this.getJobStartDate().toInstant().toEpochMilli(), DateTimeZone.forTimeZone(TimeZone.getTimeZone(zid))));
        advertc.setJobScope(this.getJobScope());
        advertc.setStatus(this.getStatus());
        advertc.setType(this.getType());
        return advertc;
    }
}
