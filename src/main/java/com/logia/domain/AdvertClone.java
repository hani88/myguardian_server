package com.logia.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.logia.service.util.CustomDateSerializer;
import org.joda.time.DateTime;

import java.util.Objects;

/**
 * Created by TREXX on 17/09/2017.
 */
public class AdvertClone {
    private static final long serialVersionUID = 1L;


    private Long id;


    private String status;


    private String jobScope;

    @JsonDeserialize (using = CustomDateSerializer.class)
    private DateTime jobStartDate;

    @JsonDeserialize (using = CustomDateSerializer.class)
    private DateTime jobEndDate;

    @JsonDeserialize (using = CustomDateSerializer.class)
    private DateTime expiry;


    private Long type;


    private UserData creator;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public AdvertClone status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJobScope() {
        return jobScope;
    }

    public AdvertClone jobScope(String jobScope) {
        this.jobScope = jobScope;
        return this;
    }

    public void setJobScope(String jobScope) {
        this.jobScope = jobScope;
    }

    public DateTime getJobStartDate() {
        return jobStartDate;
    }

    public AdvertClone jobStartDate(DateTime jobStartDate) {
        this.jobStartDate = jobStartDate;
        return this;
    }

    public void setJobStartDate(DateTime jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public DateTime getJobEndDate() {
        return jobEndDate;
    }

    public AdvertClone jobEndDate(DateTime jobEndDate) {
        this.jobEndDate = jobEndDate;
        return this;
    }

    public void setJobEndDate(DateTime jobEndDate) {
        this.jobEndDate = jobEndDate;
    }

    public DateTime getExpiry() {
        return expiry;
    }

    public AdvertClone expiry(DateTime expiry) {
        this.expiry = expiry;
        return this;
    }

    public void setExpiry(DateTime expiry) {
        this.expiry = expiry;
    }

    public Long getType() {
        return type;
    }

    public AdvertClone type(Long careType) {
        this.type = careType;
        return this;
    }

    public void setType(Long careType) {
        this.type = careType;
    }

    public UserData getCreator() {
        return creator;
    }

    public AdvertClone creator(UserData userData) {
        this.creator = userData;
        return this;
    }

    public void setCreator(UserData userData) {
        this.creator = userData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AdvertClone advert = (AdvertClone) o;
        if (advert.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, advert.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Advert{" +
            "id=" + id +
            ", status='" + status + "'" +
            ", jobScope='" + jobScope + "'" +
            ", jobStartDate='" + jobStartDate + "'" +
            ", jobEndDate='" + jobEndDate + "'" +
            ", expiry='" + expiry + "'" +
            '}';
    }

    public Advert toAdvert() {

        Advert advert = new Advert();
        if (this.getId()!=null){
            advert.setId(this.getId());
        }

        advert.setCreator(this.getCreator());
        advert.setExpiry(this.getExpiry().toGregorianCalendar().toZonedDateTime());
        advert.setJobEndDate(this.getJobEndDate().toGregorianCalendar().toZonedDateTime());
        advert.setJobStartDate(this.getJobStartDate().toGregorianCalendar().toZonedDateTime());
        advert.setJobScope(this.getJobScope());
        advert.setStatus(this.getStatus());
        advert.setType(this.getType());
        return advert;
    }
}
