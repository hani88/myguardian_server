package com.logia.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    PARENT,CARER,ADMIN,GOD
}
