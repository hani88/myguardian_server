package com.logia.domain;

/**
 * Created by TREXX on 07/06/2017.
 */
public enum Status {

    NEW, PENDING, ACTIVE, COMPLETED,
    CANCELED
}
